import Head from "next/head";
import styles from "../../styles/Home.module.css";

export default function Product({ productId, title }) {
  return (
    <div className={styles.container}>
      <Head>
        <title>{title} - My Clothing Store</title>
        <meta name="description" content={`Learn more about ${title}`} />
        <meta property="og:title" content={`${title} - My Clothing Store`} />
        <meta property="og:description" content={`Learn more about ${title}`} />
        <meta
          property="og:url"
          content={`https://myclothingstore.com/products/${productId}`}
        />
        <meta property="og:type" content="website" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <div>
          <h1 style={{ marginBottom: "15px" }} className={styles.title}>
            {title}
          </h1>
          <p>Product ID: {productId}</p>
        </div>
      </main>
    </div>
  );
}

// getStaticProps is required for dynamic routes because
// it tells Next.js what data to fetch and render at build time.
export async function getStaticProps({ params = {} } = {}) {
  return {
    props: {
      productId: params.productId,
      title: `Product ${params.productId}`,
    },
  };
}

// getStaticPaths is required for dynamic routes because
// it tells Next.js what paths should be rendered into HTML at build time.
export async function getStaticPaths() {
  const paths = [...new Array(5)].map((i, index) => {
    return {
      params: {
        productId: `${index + 1}`,
      },
    };
  });
  return {
    paths,
    fallback: false,
  };
}
